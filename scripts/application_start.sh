#!/bin/bash

# Stop all servers and start the server as a daemon
cd /home/ubuntu/carhoot-dashboard
sudo cp .env.production .env
sudo cp package.json.production package.json
sudo yarn install
sudo killall -9 node || true
sudo nohup bash -c 'yarn start' &
(sleep 3; echo; sleep 3)
